# -*- coding: utf-8 -*-

#from RPi import GPIO
import time

def go(recip):
    if recip == "Aloa Aloe":
        aloa_aloe()
    elif recip == "Bella luna":
        bella_luna()
    elif recip == "Cendrillon":
        cendrillon()
    elif recip == "Florida":
        florida()
    elif recip == "Fruit Punch":
        fruit_punch()
    elif recip == "Golden Freeze":
        golden_freeze()
    elif recip == "Grenada":
        grenada()
    elif recip == "Grenadine":
        grenadine()
    elif recip == "Orange squash":
        orange_squash()
    elif recip == "Pussy Foot":
        pussy_foot()
    elif recip == "Rainbow Color":
        rainbow_color()
    elif recip == "Virgin Zombie":
        virgin_zombie()

    
def grenadine():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(19, GPIO.OUT)
    GPIO.setup(21, GPIO.OUT)
    GPIO.setup(6,GPIO.OUT)
    GPIO.output(19, GPIO.LOW)
    GPIO.output(21, GPIO.LOW)
    GPIO.output(6, GPIO.LOW)
    time.sleep(72)
    GPIO.output(6,GPIO.HIGH)
    time.sleep(27)
    GPIO.setup(19, GPIO.HIGH)
    GPIO.setup(21, GPIO.HIGH)
    GPIO.cleanup()


def fruit_punch():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(19, GPIO.OUT)
    GPIO.setup(21, GPIO.OUT)
    GPIO.setup(20, GPIO.OUT)
    GPIO.setup(6,GPIO.OUT)
    GPIO.output(19, GPIO.LOW)
    GPIO.output(21, GPIO.LOW)
    GPIO.output(20, GPIO.LOW)
    GPIO.output(6, GPIO.LOW)
    time.sleep(18)
    GPIO.output(6, GPIO.HIGH)
    time.sleep(48)
    GPIO.output(19,GPIO.HIGH)
    time.sleep(12)
    GPIO.output(20, GPIO.HIGH)
    time.sleep(10)
    GPIO.output(21, GPIO.HIGH)
    GPIO.cleanup()


def cendrillon():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(19, GPIO.OUT)
    GPIO.setup(21, GPIO.OUT)
    GPIO.setup(20, GPIO.OUT)
    GPIO.output(19, GPIO.LOW)
    GPIO.output(21, GPIO.LOW)
    GPIO.output(20, GPIO.LOW)
    time.sleep(77)
    GPIO.setup(19, GPIO.HIGH)
    GPIO.setup(21, GPIO.HIGH)
    time.sleep(14)
    GPIO.output(20, GPIO.HIGH)
    GPIO.cleanup()


def golden_freeze():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(19, GPIO.OUT)
    GPIO.setup(21, GPIO.OUT)
    GPIO.setup(26, GPIO.OUT)
    GPIO.output(19, GPIO.LOW)
    GPIO.output(21, GPIO.LOW)
    GPIO.output(26, GPIO.LOW)
    time.sleep(30)
    GPIO.output(26, GPIO.HIGH)
    time.sleep(36)
    GPIO.setup(21, GPIO.HIGH)
    time.sleep(88)
    GPIO.setup(19, GPIO.HIGH)
    GPIO.cleanup()

def aloa_aloe():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(19, GPIO.OUT)
    GPIO.setup(21, GPIO.OUT)
    GPIO.setup(6, GPIO.OUT)
    GPIO.setup(13, GPIO.OUT)
    GPIO.output(19, GPIO.LOW)
    GPIO.output(21, GPIO.LOW)
    GPIO.output(6, GPIO.LOW)
    GPIO.output(13, GPIO.LOW)
    time.sleep(11)
    GPIO.output(21, GPIO.HIGH)
    time.sleep(79)
    GPIO.output(13, GPIO.HIGH)
    time.sleep(18)
    GPIO.output(6, GPIO.HIGH)
    time.sleep(2)
    GPIO.output(19, GPIO.HIGH)
    GPIO.cleanup()

def rainbow_color():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(19, GPIO.OUT)
    GPIO.setup(21, GPIO.OUT)
    GPIO.setup(20, GPIO.OUT)
    GPIO.setup(6, GPIO.OUT)
    GPIO.setup(13, GPIO.OUT)
    GPIO.output(19, GPIO.LOW)
    GPIO.output(21, GPIO.LOW)
    GPIO.output(20, GPIO.LOW)
    GPIO.output(6, GPIO.LOW)
    GPIO.output(13,GPIO.LOW)
    time.sleep(36)
    GPIO.output(6, GPIO.HIGH)
    time.sleep(16)
    GPIO.output(20, GPIO.HIGH)
    time.sleep(3)
    GPIO.output(21, GPIO.HIGH)
    time.sleep(11)
    GPIO.output(19, GPIO.HIGH)
    time.sleep(9)
    GPIO.output(13,GPIO.HIGH)
    GPIO.cleanup()


def bella_luna():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(19, GPIO.OUT)
    GPIO.setup(21, GPIO.OUT)
    GPIO.setup(20, GPIO.OUT)
    GPIO.output(19, GPIO.LOW)
    GPIO.output(21, GPIO.LOW)
    GPIO.output(20, GPIO.LOW)
    time.sleep(26)
    GPIO.setup(20, GPIO.HIGH)
    time.sleep(40)
    GPIO.setup(19, GPIO.HIGH)
    time.sleep(44)
    GPIO.output(21, GPIO.HIGH)
    GPIO.cleanup()

def virgin_zombie():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(19, GPIO.OUT)
    GPIO.setup(20, GPIO.OUT)
    GPIO.setup(6, GPIO.OUT)
    GPIO.output(19, GPIO.LOW)
    GPIO.output(20, GPIO.LOW)
    GPIO.output(6, GPIO.LOW)
    time.sleep(36)
    GPIO.setup(6, GPIO.HIGH)
    time.sleep(16)
    GPIO.setup(20, GPIO.HIGH)
    time.sleep(135)
    GPIO.output(19, GPIO.HIGH)
    GPIO.cleanup()

def orange_squash():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(21, GPIO.OUT)
    GPIO.setup(20, GPIO.OUT)
    GPIO.output(21, GPIO.LOW)
    GPIO.output(20, GPIO.LOW)
    time.sleep(52)
    GPIO.setup(20, GPIO.HIGH)
    time.sleep(102)
    GPIO.output(21, GPIO.HIGH)
    GPIO.cleanup()

def pussy_foot():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(21, GPIO.OUT)
    GPIO.setup(20, GPIO.OUT)
    GPIO.setup(6, GPIO.OUT)
    GPIO.output(21, GPIO.LOW)
    GPIO.output(20, GPIO.LOW)
    GPIO.output(6, GPIO.LOW)
    time.sleep(18)
    GPIO.setup(6, GPIO.HIGH)
    time.sleep(59)
    GPIO.output(21, GPIO.HIGH)
    time.sleep(105)
    GPIO.output(20, GPIO.HIGH)
    GPIO.cleanup()

def florida():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(21, GPIO.OUT)
    GPIO.setup(20, GPIO.OUT)
    GPIO.setup(6, GPIO.OUT)
    GPIO.output(21, GPIO.LOW)
    GPIO.output(20, GPIO.LOW)
    GPIO.output(6, GPIO.LOW)
    time.sleep(78)
    GPIO.setup(20, GPIO.HIGH)
    time.sleep(12)
    GPIO.output(6, GPIO.HIGH)
    time.sleep(42)
    GPIO.output(21, GPIO.HIGH)
    GPIO.cleanup()

def grenada():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(21, GPIO.OUT)
    GPIO.setup(6, GPIO.OUT)
    GPIO.output(21, GPIO.LOW)
    GPIO.output(6, GPIO.LOW)
    time.sleep(54)
    GPIO.setup(6, GPIO.HIGH)
    time.sleep(166)
    GPIO.output(21, GPIO.HIGH)
    GPIO.cleanup()

'''

window=Tk()

b1=Button(window,text="Grenadine",width=12,command=grenadine)
b1.grid(row=2,column=3)

b2=Button(window,text="Fruit Punch",width=12,command=fruit_punch)
b2.grid(row=2,column=4)

b3=Button(window,text="Cendrillon",width=12,command=cendrillon)
b3.grid(row=2,column=5)

b4=Button(window,text="Golden Freeze",width=12,command=golden_freeze)
b4.grid(row=3,column=3)

b5=Button(window,text="Aloa Aloe",width=12,command=aloa_aloe)
b5.grid(row=3,column=4)

b6=Button(window,text="Rainbow Color",width=12,command=rainbow_color)
b6.grid(row=3,column=5)

b1=Button(window,text="Bella Luna",width=12,command=bella_luna)
b1.grid(row=4,column=3)

b2=Button(window,text="Virgin Zombie",width=12,command=virgin_zombie)
b2.grid(row=4,column=4)

b3=Button(window,text="Orange Squash",width=12,command=orange_squash)
b3.grid(row=4,column=5)

b4=Button(window,text="Pussy Foot",width=12,command=pussy_foot)
b4.grid(row=5,column=3)

b5=Button(window,text="Florida",width=12,command=florida)
b5.grid(row=5,column=4)

b6=Button(window,text="Grenada",width=12,command=grenada)
b6.grid(row=5,column=5)




window.mainloop()
'''