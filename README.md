# Smart Mix

Le code source de ce projet a été développé pour la conception d'une machine à cocktails 
autonome dans le cadre de mes études en école d'ingénieur. En équipe de 5 nous avons 
décidé de choisir de coder en Python. Python ne semble pas être le meilleur choix pour
ce genre de projet (en IOT on pense plus à C ou C++) . Mais l'avantage de Python , c'est
qu'il permet le développement rapide de prototype mais le plus important, n'ayant jamais 
codé en Python , cela nous semblait une occasion de découvrir et apprendre un nouveau langage. 

<img src="/images/prototype.jpg"  width="300" height="400">

Si ce projet vous intéresse, voici les différentes étapes du projet de notre machine à cocktails.
Dans notre cas, nous avons choisi de le faire avec 6 bouteilles mais il est possible de le faire avec plus ou moins.

## ATTENTION

La documentation qui suit est un retour d'expérience d’un projet étudiant et a pour but de montrer le déroulement du projet et non un tutoriel. 
Lors de la conception nous avons eu besoin de manipuler des tensions allant jusqu'à 240 volts. Un étudiant dans notre équipe possédait une formation 
sur la manipulation et la sécurité de matériels électriques qui nous a permis de concevoir notre prototype totalement en sécurité car 
il est TRÉS dangereux de manipuler de l'électricité près de liquide.

Il est donc déconseillé de reproduire un prototype si vous ne possédez pas les compétences nécessaires. ET dans le cas contraire si vous essayez de 
reproduire le prototype, je me décharge de toute responsabilité, vous et vous seul devrez assumer les conséquences en cas de problèmes.


## Liste des produits pour le prototype

- 6 pompes péristaltiques (il est préférable de prendre des pompes à engrenages pour un 
    débit plus constant et plus rapide)
- 1 Raspberry 3 B+
- 1 Ecran tactile pour Raspberry
- 1 Carte Relais 6/8 canaux
- 6 Bouteilles Soda Stream
- Alimentation insdustrielle (pour convertir tension alternative de 240 Volts en une tension continue de 12 Volts)
- Fils électriques
- Tubes en silicone


## La structure de la machine 

En nous inspirant du fonctionnement des appareils de la marque Soda Stream et notamment de leur poignée, nous avons décidé d’utiliser un principe de poignée légèrement plus large que la bouteille.

<img src="/images/poigne.png"  width="100" height="250">

Pour le choix des matériaux, nous avons décidé d’utiliser du bois car, ayant décidé de construire notre machine par nos propres moyens, il nous fallait utiliser un matériel facilement usinable et assez peu coûteux. Puisque nous avons surtout utilisé du bois de récupérations la machine était très imposante et très lourde.

Voici donc la modélisation de la structure: 

<img src="/images/modelisation.png"  width="300" height="250">

Sur la face avant vous pouvez voir deux rectangles : : le plus grand, permet de position un verre et le deuxième, plus petit, permet de fixer l'écran tactile. 
Derrière ce deuxième écran se trouve une étagère qui permet de positionner toute la partie électronique et mécanique de notre machine. Nous reviendrons sur ce point plus tard.

<img src="/images/pieces.png"  width="280" height="232">

## L'électronique de la machine 

Pour l'électronique de la machine , voici le schéma qui résume les connexions hors écran tactile.

<img src="/images/schema.png"  width="700" height="540">

En temps normal, l’écran se branche sur l’ensemble des pins de la Raspberry mais il ne les utilise pas tous. 
Cette configuration était impossible pour nous car nous avons aussi besoin de brancher la carte relais. 
C’est pour cela que nous avons utilisé une « electronic board » afin de pouvoir redistribuer un courant électrique sur plusieurs ports. 
L’écran tactile a besoin de 3.3 et 5 Volts pour fonctionner, c’est pour cela que nous avons relié les pins 3.3 et 5 Volts sur « l’ electronic board » afin de pouvoir les réutiliser avec la carte relais. 


Avant de souder et fixer nous avons du étuider le positionnement de nos composants pour optimiser le câblage.
Et voici une vue de "l'étagère électronique" positionné dans notre machine : 
 

<img src="/images/etagere.jpg"  width="400" height="300">

## Le logiciel de la machine

Le code que vous trouverez dans ce projet git, permet simplement de tester notre machine, tester des cocktails, tester l'écran tactile. Avec le peu de temps que nous avions 
il était impossible pour nous de concevoir entièrement le logiciel voulu. Pour résumer ce code permet juste de faire une démonstration de notre projet.Mais vous trouverez aussi des
éléments du logiciel voulu à terme.

Les différentes fenêtres de l'interface graphique:

<img src="/images/interface.png"  width="600" height="350">

Diagramme de séquence:

<img src="/images/sequence.png"  width="400" height="300">
