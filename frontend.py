# -*- coding: utf-8 -*-

import tkinter as tk
import tkinter.ttk as ttk

from cmdbdd import *
from utils.recip_exist import *
from GPIO.GPIO import *

#-------Sert pour la selection des liquide dans remplir-------
globlist = ["Jus d'ananas", "Jus d'orange", "Jus de citron", "Sirop de grenadine", "Jus de Mangue", "Sirop de menthe"]
globvar = ""
globtype = 2


def get_globtype():
    global globtype
    return globtype
    
def get_globvar():
    global globvar
    return globvar

def get_globlist(i):
    global globlist
    return globlist[i]
    
#En dur
def set_globtype():
    global globtype 
    globtype = 1

def set_globvar(value):
    global globvar
    globvar = value

def set_globlist(i,value):
    global globlist
    globlist[i] = value
    
    
def combine_funcs(*funcs):
    def combined_func(*args, **kwargs):
        for f in funcs:
            f(*args, **kwargs)
    return combined_func

def tuple2var(a):
    a = a[0]
    return a


class SampleApp (tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        container = tk.Frame(self)
        container.grid(row=0, column=0)

        self.frames = {}
        for F in (MenuPrincipal, Cocktail, Preparation, ProgressBar, Reservoir, Liquide, RechercheA, RechercheS, Reservoir1,
                  ProgressBar1, TypeRecherche, RechercheCk, Recherche1, ListeProduit):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("MenuPrincipal")

    def show_frame(self, page_name):
        frame = self.frames[page_name]
        frame.tkraise()


class MenuPrincipal(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        label = tk.Label(self, text="SMARTMIX", width=12)
        label.grid(row=0, column=3)

        button1 = tk.Button(self, text="Préparer", width=12,
                            command=lambda: controller.show_frame("Cocktail"))
        button1.grid(row=2, column=2)

        button2 = tk.Button(self, text="Remplir", width=12,
                            command=lambda: controller.show_frame("Reservoir"))
        button2.grid(row=2, column=4)

        button3 = tk.Button(self, text="Nettoyage", width=12,
                            command=lambda: controller.show_frame("Reservoir1"))
        button3.grid(row=4, column=2)

        button4 = tk.Button(self, text="Recherche", width=12,
                            command=lambda: controller.show_frame("TypeRecherche"))
        button4.grid(row=4, column=4)


class Cocktail(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        button = tk.Button(self, text="Retour", width=12,
                           command=lambda: controller.show_frame("MenuPrincipal"))
        button.grid(row=0, column=0)

        row = 0
        i = 1
        column = 0
        
        
        '''dict = getRecettes()

        global globlist
        
        recip = recip_faisable(globlist, dict)'''
        
        recip = {
            "Aloa Aloe":[1,3,5,4],
            "Bella luna":[2,1,3],
            "Cendrillon":[1,2,3],
            "Florida":[2,3,5],
            "Fruit Punch":[1,2,3,5],
            "Golden Freeze":[1,3,6],
            "Grenada":[2,5],
            "Grenadine":[2,5,1],
            "Orange squash":[2,3,8],
            "Pussy Foot":[2,3,5,7],
            "Rainbow Color":[2,1,4,3,5],
            "Virgin Zombie":[1,3,5]
            }

        for num in recip.keys() :
            btn = tk.Button(self, text=num, width=12,
                            command=lambda: combine_funcs(set_globvar(num),controller.show_frame("Preparation")))
            btn.grid(row=i, column=column)
            column += 1
            row += 1
            if row == 3:
                i += 1
                row = 0
                column = 0


class Preparation(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        

        button = tk.Button(self, text="Retour", width=12,
                           command=lambda: controller.show_frame("Cocktail"))
        button.grid(row=0, column=0)

        label = tk.Label(self, text="Ajoutez les glaçons puis lancez la préparation !")
        label.grid(row=3, column=3)

        button1 = tk.Button(self, text="Lancer", width=12,
                            command=lambda: combine_funcs(go(get_globvar()),controller.show_frame("ProgressBar")))
        button1.grid(row=4, column=3)


class ProgressBar(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        button1 = tk.Button(self, text="Retour", width=12,
                            command=lambda: controller.show_frame("Cocktail"))
        button1.grid(row=0, column=0)

        button = tk.Button(self, text="FIN", width=12)
        button.grid(row=5, column=3)

        button["state"] = "disable"

        progress = ttk.Progressbar(self, orient="horizontal", length=100, mode="determinate")
        progress.grid(row=3, column=2, columnspan=3)


class Reservoir(tk.Frame):
    
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        button = tk.Button(self, text="Retour", width=12,
                           command=lambda: controller.show_frame("MenuPrincipal"))
        button.grid(row=2, column=0)
        
        text1 = tk.StringVar()
        text2 = tk.StringVar()
        text3 = tk.StringVar()
        text4 = tk.StringVar()
        text5 = tk.StringVar()
        text6 = tk.StringVar()
        ltext = [text1, text2, text3, text4, text5, text6]
        
        for i in range(6):
            ltext[i].set(get_globlist(i))
        
        button1 = tk.Button(self, textvariable=text1, width=12,
                            command=lambda: controller.show_frame("Liquide"))

        button1.grid(row=2, column=2)

        button2 = tk.Button(self, textvariable=text2, width=12)
        button2.grid(row=2, column=4)

        button3 = tk.Button(self, textvariable=text3, width=12)
        button3.grid(row=4, column=2)

        button4 = tk.Button(self, textvariable=text4, width=12)
        button4.grid(row=4, column=4)

        button5 = tk.Button(self, textvariable=text5, width=12)
        button5.grid(row=5, column=2)

        button6 = tk.Button(self, textvariable=text6, width=12)
        button6.grid(row=5, column=4)

        refresh = tk.Button(self, text="Refresh", width=12,
                            command=lambda: ltext[0].set(get_globlist(0))).grid(row=7, column=4)
   
    
 
        
class Liquide(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        button = tk.Button(self, text="Retour", width=12,
                           command=lambda: controller.show_frame("Reservoir"))
        button.grid(row=2, column=0)
        
        #-----------Ici on essaie d'envoyer une valeur ---------
        
        button1 = tk.Button(self, text="Alcool", width=12,
                            command=lambda *args : controller.show_frame("RechercheA"))
        button1.grid(row=2, column=2)

        button2 = tk.Button(self, text="Soft", width=12,
                            command=lambda *args : controller.show_frame("RechercheS"))
        button2.grid(row=2, column=4)
        



class RechercheA(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        
        self.type = 1

        button = tk.Button(self, text="Retour", width=12,
                           command=lambda: controller.show_frame("Reservoir"))
        button.grid(row=0, column=0)

        e1 = tk.Entry(self)
        e1.grid(row=1, column=0, columnspan=3)

        list1 = tk.Listbox(self, height=6, width=35 )
        
        
        ing = getIngredients(1)
        
        for i in range(len(ing)):
            list1.insert(i,ing[i])
            

        list1.grid(row=2, column=0, rowspan=6, columnspan=2)

        sb1 = tk.Scrollbar(self)
        sb1.grid(row=2, column=2, rowspan=6)
        list1.configure(yscrollcommand=sb1.set)
        sb1.configure(command=list1.yview)

        b2 = tk.Button(self, text="Search", width=12)
        b2.grid(row=1, column=4)

        b3 = tk.Button(self, text="Select", width=12, command=lambda *args: combine_funcs(set_globlist(0,list1.get(tuple2var(list1.curselection()))), controller.show_frame("Reservoir")))        
        b3.grid(row=2, column=4)
        
  
    
class RechercheS(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        
        self.type = 1

        button = tk.Button(self, text="Retour", width=12,
                           command=lambda: controller.show_frame("Reservoir"))
        button.grid(row=0, column=0)

        e1 = tk.Entry(self)
        e1.grid(row=1, column=0, columnspan=3)

        list1 = tk.Listbox(self, height=6, width=35 )
        
        ing = getIngredients(2)
        
        for i in range(len(ing)):
            list1.insert(i,ing[i])

        list1.grid(row=2, column=0, rowspan=6, columnspan=2)

        sb1 = tk.Scrollbar(self)
        sb1.grid(row=2, column=2, rowspan=6)
        list1.configure(yscrollcommand=sb1.set)
        sb1.configure(command=list1.yview)

        b2 = tk.Button(self, text="Search", width=12)
        b2.grid(row=1, column=4)

        b3 = tk.Button(self, text="Select", width=12, command=lambda *args: combine_funcs(set_globlist(0,list1.get(tuple2var(list1.curselection()))), controller.show_frame("Reservoir")))
        b3.grid(row=2, column=4)
    


class Reservoir1(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        button = tk.Button(self, text="Retour", width=12,
                           command=lambda: controller.show_frame("MenuPrincipal"))
        button.grid(row=2, column=0)

        button1 = tk.Button(self, text="Reservoir 6", width=12,
                            command=lambda: controller.show_frame("ProgressBar1"))

        button1.grid(row=2, column=2)

        button2 = tk.Button(self, text="Reservoir 5", width=12)
        button2.grid(row=2, column=4)

        button3 = tk.Button(self, text="Reservoir 4", width=12)
        button3.grid(row=4, column=2)

        button4 = tk.Button(self, text="Reservoir 3", width=12)
        button4.grid(row=4, column=4)

        button5 = tk.Button(self, text="Reservoir 2", width=12)
        button5.grid(row=5, column=2)

        button6 = tk.Button(self, text="Reservoir 1", width=12)
        button6.grid(row=5, column=4)


class ProgressBar1(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        button1 = tk.Button(self, text="Retour", width=12,
                            command=lambda: controller.show_frame("Reservoir1"))
        button1.grid(row=0, column=0)

        button = tk.Button(self, text="FIN", width=12)
        button.grid(row=5, column=3)

        button["state"] = "disable"

        progress = ttk.Progressbar(self, orient="horizontal", length=100, mode="determinate")
        progress.grid(row=3, column=2, columnspan=3)


class TypeRecherche(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        label = tk.Label(self, text="Choisissez un type de recherche", width=25)
        label.grid(row=0, column=3)

        button1 = tk.Button(self, text="Alcool", width=12,
                            command=lambda: controller.show_frame("Recherche1"))
        button1.grid(row=2, column=2)

        button2 = tk.Button(self, text="Soft", width=12,
                            command=lambda: controller.show_frame("Recherche1"))
        button2.grid(row=2, column=4)

        button3 = tk.Button(self, text="Cocktail", width=12,
                            command=lambda: controller.show_frame("RechercheCk"))
        button3.grid(row=4, column=2)

        button4 = tk.Button(self, text="Ingredient", width=12,
                            command=lambda: controller.show_frame("Recherche1"))
        button4.grid(row=4, column=4)


class RechercheCk(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        button = tk.Button(self, text="Retour", width=12,
                           command=lambda: controller.show_frame("TypeRecherche"))
        button.grid(row=0, column=0)
        
        
        e1 = tk.Entry(self)
        e1.grid(row=1, column=0, columnspan=3)
        
        #Récup recettes dans une liste recettes
        recip = getRecettes()
        recette = []
        for i in recip.keys():
            recette.append(i)
        #---------------------------------------
        
        list1 = tk.Listbox(self, height=6, width=35)
        
        for i in range(len(recette)):
            list1.insert(i,recette[i])
            
        list1.grid(row=2, column=0, rowspan=6, columnspan=2)

        sb1 = tk.Scrollbar(self)
        sb1.grid(row=2, column=2, rowspan=6)
        list1.configure(yscrollcommand=sb1.set)
        sb1.configure(command=list1.yview)

        b2 = tk.Button(self, text="Search", width=12)
        b2.grid(row=1, column=4)

        b3 = tk.Button(self, text="Select", width=12,
                       command=lambda: combine_funcs(set_globvar(list1.get(tuple2var(list1.curselection()))),controller.show_frame("Preparation")))
        b3.grid(row=2, column=4)


class Recherche1(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        button = tk.Button(self, text="Retour", width=12,
                           command=lambda: controller.show_frame("TypeRecherche"))
        button.grid(row=0, column=0)

        e1 = tk.Entry(self)
        e1.grid(row=1, column=0, columnspan=3)

        list1 = tk.Listbox(self, height=6, width=35)
        list1.grid(row=2, column=0, rowspan=6, columnspan=2)

        sb1 = tk.Scrollbar(self)
        sb1.grid(row=2, column=2, rowspan=6)
        list1.configure(yscrollcommand=sb1.set)
        sb1.configure(command=list1.yview)

        b2 = tk.Button(self, text="Search", width=12)
        b2.grid(row=1, column=4)

        b3 = tk.Button(self, text="Select", width=12,
                       command=lambda: controller.show_frame("ListeProduit"))
        b3.grid(row=2, column=4)


class ListeProduit(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        label = tk.Label(self, text="Voici la liste des produits:")
        label.grid(row=3, column=3)

        button1 = tk.Button(self, text="OK", width=12,
                            command=lambda: controller.show_frame("Recherche1"))
        button1.grid(row=4, column=3)

'''

if __name__ == "__main__":
    app = SampleApp()
    app.mainloop()

'''