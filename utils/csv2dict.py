# -*- coding: utf-8 -*-
 
    
import csv

def csv2dict(fcsv):

    with open(fcsv, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';')
        i=[]
        for row in spamreader:
            i.append(row)
            
    dict={}
    
    for n in range(len(i)):
        dict[i[n][0]]=(i[n][1],i[n][2])
    
    return dict
    
def csv2list(fcsv):

    with open(fcsv, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';')
        i=[]
        for row in spamreader:
            i.append(row)
            
    
    return i
    





'''------------------------------------------

ECRIRE DU CSV

import csv
with open('eggs.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=';')
    spamwriter.writerow(['Spam'] * 5 + ['Baked Beans'])
    spamwriter.writerow(['Spam', 'Lovely Spam', 'Wonderful Spam'])

--------------------------------------------------'''