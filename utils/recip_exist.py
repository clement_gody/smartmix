# -*- coding: utf-8 -*-

def recip_exist(l, d):

    q = {}
    pt = 0
    
    if isinstance(l, list) == True :
        

    
        for j in d.keys():
            
            n=0
            
            for i in range(len(l)) :
                    
                if l[i] in d[j] :
                        
                    n += 1
                        
            if n == len(l) :
                
                q[j] = d[j]
        
    
    elif isinstance(l, str) == True :
        
        
        for j in d.keys() :
            
            if l in d[j] :
            
                q[j] = d[j]

    else :

        q = 0
    
    return q


def recip_class(l, d):

    q = {}
    pt = 0
    
    #6/6 - 5/6 - 4/6 - 3/6 - 2/6 - 1/6 - 0/6
    cl = [ [] , [] , [] , [] ,[] , [] ]
    
    if isinstance(l, list) == True :
        

    
        for j in d.keys():
            
            t=0
            n=0
            
            for i in range(len(l)) :
                    
                if l[i] in d[j] :
                        
                    n += 1
            
            for k in range(len(l)):
                        
                if n == len(l)-k :
                
                    cl[t].append(j)
                
                t += 1
        
        for j in range(len(cl)) :
            for i in range(len(cl[j])) :
                
                q[cl[j][i]] = d[cl[j][i]]
            

        
    
    elif isinstance(l, str) == True :
        
        
        for j in d.keys() :
            
            if l in d[j] :
            
                q[j] = d[j]

    else :

        q = 0
    
    return q


def recip_faisable(l, d):
    q = {}
    for i in d.keys():
        n = 0
        for j in d[i]:
            if j in l:
                n += 1
        if n == len(d[i]):
            q[i] = d[i]

    return q


'''
#----------------TESTS-----------------

recipe = { 1 : ('rhum','banane','coco') , 2 : ('rhum','coca','vodka') , 3 : ('vodka','citron','orange', 'pamplemous'), 4 : ('whisky', 'coca', 'orange','citron')}

liste = ['rhum','banane','vodka','orange','citron']

resu = recip_faisable(liste, recipe)
print(resu)


recipe = { 1 : ('rhum','banane','coco') , 2 : ('rhum','coca','vodka') , 3 : ('vodka','citron','orange', 'pamplemous'), 4 : ('whisky', 'coca', 'orange','citron')}

search = 'orange vodka citron'  #Ici on va simplement prendre une recherche et la convertir en liste 

liste = search.split()  #Et puis si jamais on veut finalement rajouter un truc voilà quoi

liste.append('pamplemous')

resu = recip_class(liste, recipe)
print(resu)

var = 'citron'

a = recip_exist(liste, recipe)
print(a)

a = recip_exist(var, recipe)
print(a)

os.system("pause")

-----------------------------------------------'''
