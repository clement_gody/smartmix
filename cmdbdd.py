# -*- coding: utf-8 -*-

import sqlite3

def getRecettes():
    
    conn = sqlite3.connect('ma_base.db')
    
    cursor = conn.cursor()
    
    dico = {}
    
    for n in range(1,13):
    
        p = [n,n,n,n,n,n,n]
        
        cursor.execute("""
        SELECT nom
        FROM Recettes
        WHERE id = ?
        UNION
        SELECT nom 
        FROM Ingrédients 
        WHERE id = (
            SELECT i1 
            FROM Recettes 
            WHERE id = ?)
        UNION
        SELECT nom
        FROM Ingrédients
        WHERE id = (
            SELECT i2
            FROM Recettes
            WHERE id = ?)
        UNION
        SELECT nom
        FROM Ingrédients
        WHERE id = (
            SELECT i3
            FROM Recettes
            WHERE id = ?)
        UNION
        SELECT nom
        FROM Ingrédients
        WHERE id = (
            SELECT i4
            FROM Recettes
            WHERE id = ?)
        UNION
        SELECT nom
        FROM Ingrédients
        WHERE id = (
            SELECT i5
            FROM Recettes
            WHERE id = ?)
        UNION
        SELECT nom
        FROM Ingrédients
        WHERE id = (
            SELECT i6
            FROM Recettes
            WHERE id = ?)""", p )
            
        r = cursor.fetchall()
        
        nom = r[0][0]
        
        ing = []
        # J'ai volontairement init à 1 pour ne pas afficher mojito caca
        for m in range(1,len(r)): 
            ing.append(r[m][0])
        
        dico[nom] = ing
    
    conn.close()
    
    
    del dico["Jus d'ananas"]
    
    return dico

#dico = getRecettes()
#print(dico)

#------Rappel : Alcool = 1, Soft = 2, Elem = 3 -------

def getIngredients(type) :
    
    ltype = [type]
    
    conn = sqlite3.connect('ma_base.db')
    cursor = conn.cursor()
    
    cursor.execute("""
    SELECT nom 
    FROM Ingrédients
    WHERE type = ?""", ltype )
    
    l = cursor.fetchall()
    
    liste = []
    
    for i in range(len(l)) :
        liste.append(l[i][0])
    
    return liste

#resu = getIngredients(2)
#print(resu)

'''cursor.execute("""SELECT type, nom FROM Ingrédients""")
i1 = cursor.fetchone()
print(i1)'''

