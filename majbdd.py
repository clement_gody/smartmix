# -*- coding: utf-8 -*-

import sqlite3
from utils.csv2dict import csv2list

def majbdd(fcsvR, fcsvI):

#Création d'une connexion à la base de donnée 
    conn = sqlite3.connect('ma_base.db')

#Déclaration d'un curseur qui nous permettra d'écrire des requetes
    cursor = conn.cursor()

    cursor.execute("""
    DROP TABLE IF EXISTS Recettes
    """)
    conn.commit()
#conn.commit() = enregistrement des modifications 
    
    cursor.execute("""
    DROP TABLE IF EXISTS Ingrédients
    """)
    conn.commit()

#Création de la table Recettes
    cursor.execute("""
    CREATE TABLE Recettes (
        id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
        nom TEXT,
        i1 INTEGER,
        i2 INTEGER,
        i3 INTEGER,
        i4 INTEGER,
        i5 INTEGER,
        i6 INTEGER,
        q1 INTEGER,
        q2 INTEGER,
        q3 INTEGER,
        q4 INTEGER,
        q5 INTEGER,
        q6 INTEGER
        
    )
    """)
    conn.commit()


    cursor.execute("""
    CREATE TABLE Ingrédients (
        id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
        type INTEGER,
        nom TEXT
    )
    """)
    conn.commit()
    
    recet = csv2list(fcsvR)
    
    cursor.executemany("""
    INSERT INTO Recettes (nom,i1,i2,i3,i4,i5,i6,q1,q2,q3,q4,q5,q6) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)""", recet)
    conn.commit()
    
    ing = csv2list(fcsvI)
    
    cursor.executemany("""
    INSERT INTO Ingrédients (type, nom) VALUES (?,?)""", ing)
    conn.commit()
    
    
    conn.close()
    
majbdd('Recettes1.csv', 'Ingrédients.csv')
